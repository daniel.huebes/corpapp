'use strict'
const AWS = require('aws-sdk');

exports.handler = async (event) => {
    let userId = 0;
    let id = 0;
    
    // Pega o termo buscado
    if (event.body){ 
        userId = JSON.parse(event.body).userId;
        id = JSON.parse(event.body).id;
    } else { 
        userId = event.userId;
        id = event.id;
    }
    
    // Inicializa variáveis para consulta no DynamoDB
    const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08" });
    const documentClient = new AWS.DynamoDB.DocumentClient();
    
    let date = new Date();
    const params = {
        TableName: "favBooks",
        Item: {
          "createdDate": date.toISOString(),
          "isbn": isbn,
          "userId": userId
        }
    }
    
    try {
        const data = await documentClient.put(params).promise();
        //console.log(data);
    } catch (e) {
        console.log(e);
    }
    
    
    // TODO implement
    const response = {
        statusCode: 200,
        headers: { "Access-Control-Allow-Origin" : "*" },
        body: JSON.stringify('OK'),
    };
    return response;
};
