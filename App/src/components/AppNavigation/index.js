import React, { Component, useState } from 'react';
import { TouchableOpacity, StyleSheet, View} from 'react-native';
import { Feather } from '@expo/vector-icons'

import { createStackNavigator } from '@react-navigation/stack';

import ToBuyList from '../ToBuyList';
import Profile from '../Profile';
import BookDetail from '../BookDetail';
import ListOfListScreen from '../ListOfListScreen';
import List from '../ListOfListScreen/List';
import Tabs from '../Tabs';  

import Amplify, { Auth } from 'aws-amplify';
import amplifyConfig from '../../amplify-config';
Amplify.configure(amplifyConfig);

function toBuyList({ navigation }) {
    return (
        <View>
            <ToBuyList navigation={navigation}></ToBuyList>
        </View>
    );
}

const Stack = createStackNavigator();
export default class AppNavigation extends Component {
    state = {
        showAllBooks: false
    }

    logout = async () => {
        Auth.signOut({ global: true })
            .then((data) => this.props.handleClearSession())
            .catch(err => console.log(err));
    }

    render() {
        return (
            <>    
                <Stack.Navigator initialRouteName="Aniversariantes">
                    <Stack.Screen 
                    name="Aniversariantes" 
                    component={toBuyList} 
                    options={{
                        headerTitleAlign: 'center', 
                        /*headerLeft: () => (
                            <TouchableOpacity>
                            <Feather 
                                name="book-open" 
                                size={28} 
                                style={[styles.showAllBookHeaderIcon, this.state.showAllBooks ? {color: '#0D0D0D'} : {color: '#bfbfbf'} ]} 
                                onPress={() => this.setState({ showAllBooks : !this.state.showAllBooks})}/>
                            </TouchableOpacity>
                        ),
                        headerRight: () => (
                            <TouchableOpacity>
                            <Feather name="log-out" size={28} style={styles.logouHeaderIcon} onPress={this.logout}/>
                            </TouchableOpacity>
                        )*/
                    }}
                    />
        
                    <Stack.Screen name="Notificações" component={ListOfListScreen} 
                    options={ 
                        {headerLeft: null,
                        headerTitleAlign: 'center'}       
                    }
                    extra
                    />
        
                    <Stack.Screen name="Marcações" component={List} 
                    options={ 
                        {headerLeft: null,
                        headerTitleAlign: 'center'}
                    }
                    />
                    
                    <Stack.Screen name="Usuário" component={Profile} 
                        options={ 
                            {headerLeft: null,
                            headerTitleAlign: 'center'}
                        }
                        initialParams={{ handleClearSession: this.props.handleClearSession }}
                    />
                </Stack.Navigator>
                <Tabs nav={this.props.navigationRef}></Tabs>
            </>
        );
    }    
}

const styles = StyleSheet.create({
  showAllBookHeaderIcon: {
    marginLeft: 20
  },

  logouHeaderIcon: {
    marginRight: 20,
  }
});