import React, { Component } from 'react';
import { View, StyleSheet, Text, FlatList, 
        ActivityIndicator, Dimensions, AsyncStorage, 
        TouchableOpacity } from 'react-native';   
import ListItem from '../ListItem';
import api from '../../services/api';
import SubHeader from '../SubHeader';
import Moment from 'moment';
import { Feather } from '@expo/vector-icons';

export default class ToBuyList extends Component {
    state = {
        itensToBuy : [],
        listEmployee : [],
        errorMessage : null,
        infoMessage : null,
        loading: false,
        loadingInfinite: false,
        total: 1
    };
    _isMounted = false;

    constructor(props) {
        super(props);
        //this._bootstrapAsync();
    }

	_bootstrapAsync = async () => {
		try {            
            this.loadItensToBuy();
		} catch (error) {
			console.log('Error getting user:',error);
		}
    }

    componentDidMount() {
        this._isMounted = true;

        this._unsubscribe = this.props.navigation.addListener('focus', () => {      
            if (this._isMounted) {
                this.loadItensToBuy();
            }
        });          
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    
    loadItensToBuy = async () => {
        this.setState({ errorMessage: null});
        //await this.setState({ listEmployee: []});
        if (this.state.listEmployee.length == 0){
            this.setState({ loading: true });
            try {
                
                let url = 'https://hvlalm1b27.execute-api.us-east-1.amazonaws.com/default/CorpApp-ListBirthDay';
                let data = await api.get(url);
                
                this.setState({ listEmployee: data.data.content});
                //this.setState({ listEmployee: []});
                this.setState({ loading: false });
            } catch (error) {
                //console.log(error.originalError.message);
                this.setState({ errorMessage: error.originalError.message});
                this.setState({ loading: false });
            }
        }
    }              
    /*                    
    <ListItem 
        key={item.id} 
        item={item} 
        navigation={this.props.navigation} 
    />      */  

    render() {
        return (
            <View style={styles.global}>
                <View style={styles.toBuyListContainer}>
                    <FlatList                                                  
                        data={this.state.listEmployee}  
                        keyExtractor={item => String(item.employeeId)}
                        numColumns={1}
                        bounces={false}
                        renderItem={({item: item}) => (                                
                            <View style={styles.container}>      
                                                    
                                <SubHeader 
                                    key={item.employeeId} 
                                    image={item.photoLink}
                                    title={item.name} 
                                    autor={item.department} 
                                    date={item.birthday} 
                                    navigation={this.props.navigation} 
                                /> 
                            </View>
                        )}
                    />
                </View>    


                { this.state.listEmployee.length == 0 && !this.state.loading && !this.state.errorMessage &&
                    <Text style={styles.info}>Nenhuma aniversariante no período</Text>
                }
                
                { this.state.errorMessage && 
                    <View style={styles.errorContainer}>
                        <Text style={styles.error}>{this.state.errorMessage}</Text>

                        <TouchableOpacity 
                            onPress={this.loadItensToBuy}
                            style={styles.refreshContainer}>
                            <Feather name="refresh-cw" size={20}/>
                            <Text style={{ fontSize: 20, marginLeft: 6 }}>Tentar novamente</Text>
                        </TouchableOpacity>

                    </View>    
                }
                { this.state.infoMessage &&
                    <Text style={styles.info}>{this.state.infoMessage}</Text>
                }

                { this.state.loading &&
                    <View style={styles.containerSpinner}>     
                    </View>    
                }
                
                { this.state.loading && 
                    <ActivityIndicator 
                        style={styles.spinner}
                        size="large" 
                        color="#000000" 
                    />       
                }
            </View>
        )    
    }
}

const styles = StyleSheet.create({
    global: {
        height: Dimensions.get("window").height ,
        backgroundColor: '#F8F8FA',
    },
    container: {
        marginLeft: 0,
        marginRight: 0,
    },
    toBuyListContainer: {
        marginTop: 15,
        marginBottom: 50
    },
    info: {
        padding: 15,
        justifyContent: "center",
        textAlign: "center",
        borderRadius: 30,
        fontSize: 18
    },   

    containerSpinner: {
        backgroundColor: '#000000',
        opacity: 0.2,
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
    },

    spinner: {
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
        justifyContent: 'center', 
        alignItems: 'center',
        opacity: 1 ,
        marginBottom: 170,
    },


    refreshContainer: {
        marginTop: 10,
        flexDirection: "row",
        justifyContent: "center",
    },

    errorContainer: {
        padding: 15,
        width: '100%',
        alignItems: "center",
        backgroundColor: '#e37f7f',
        justifyContent: "center",
        marginTop: 49
    },

    error: {
        padding: 15,
        justifyContent: "center",
        textAlign: "center",
        borderRadius: 30,
        fontSize: 18
    }, 
});