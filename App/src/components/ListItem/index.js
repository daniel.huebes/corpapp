import React, { useState } from 'react';
import { View, Image, StyleSheet, Text, Dimensions } from 'react-native';
import { CheckBox } from 'react-native-elements'
import { Feather } from '@expo/vector-icons';

export default function ListItem ({ item, navigation, canDelete }) {
    const [checked, setChecked] = useState(false);

    return (
        <View style={styles.container}>

            <CheckBox
                //center
                title={item.nameItem}
                checked={checked}
                onPress={() => setChecked(!checked)}
                style={styles.nameItem}
                textStyle={
                    {fontSize: 20}
                }
                containerStyle={
                    {
                        borderColor: '#F8F8FA',
                        backgroundColor: '#F8F8FA',
                        padding: 0,
                        paddingLeft: 5,
                        marginTop: 0,
                        marginBottom: 0,
                    }
                }
            />
            
            { canDelete &&
                <View style={styles.deleteContainer}>
                    <Feather name="trash-2" size={16} style={styles.iconDelete}/>
                </View>
            }
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        marginTop: 0,
        flexDirection: "row",
    }, 

    nameItem: {
        fontSize: 28
    },

    deleteContainer: {
        flex: 1,
        alignItems: "flex-end",
    },

    iconDelete: {

    }
});

//export default ListItem;