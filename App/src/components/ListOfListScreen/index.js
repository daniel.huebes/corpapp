import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, FlatList, TextInput,
        ActivityIndicator, Dimensions, AsyncStorage, 
        TouchableOpacity } from 'react-native';   
import Button from '../Button';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Feather } from '@expo/vector-icons';
import api from '../../services/api';
import Moment from 'moment';

export default function ListOfListScreen ({ item, navigation }) {
    const [notifications, setNotifications] = useState([]);
    const [loading, setLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState(null);

    async function loadUserData() {
        setLoading(true);
        setErrorMessage(null);
        try {
            let data = await api.get('https://bwhpcxvfw5.execute-api.us-east-1.amazonaws.com/default/CorpApp-ListNotification');
            await setNotifications(data.data.content);
            //await setNotifications([]);  
            setLoading(false);
        } catch (error) {     
            setErrorMessage(error.originalError.message);    
            setLoading(false);
        }        
    }

    useEffect(() => {
        loadUserData();
    }, []);

    return (
        <View style={styles.global}>
            <View style={styles.dataContainer}>
                <FlatList                                                  
                    data={notifications.notifications}  
                    keyExtractor={item => String(item.notificationID)}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item: item}) => (            
                        <View style={styles.profileInfo}>
                            <Text style={[ item.read ? styles.nameRead : styles.name ]}>{item.notificationSubject}</Text>
                            <Text style={styles.data}>{Moment(item.creationDate).format('DD/MM/YYYY HH:mm')}</Text>
                            <Text style={styles.bio}>{item.notificationContent}</Text>
                        </View>
                    )}
                />
            </View>

            { errorMessage && 
                <View style={styles.errorContainer}>
                    <Text style={styles.error}>{errorMessage}</Text>

                    <TouchableOpacity 
                        onPress={loadUserData}
                        style={styles.refreshContainer}>
                        <Feather name="refresh-cw" size={20}/>
                        <Text style={{ fontSize: 20, marginLeft: 6 }}>Tentar novamente</Text>
                    </TouchableOpacity>

                </View>    
            }

            { !notifications.notifications && !loading &&
                <Text style={styles.info}>Nenhuma notificação disponível</Text>
            }
            
            { loading &&
                <View style={styles.containerSpinner}>     
                </View>    
            }
            
            { loading && 
                <ActivityIndicator 
                    style={styles.spinner}
                    size="large" 
                    color="#000000" 
                />       
            }
        </View>
    )
}


const styles = StyleSheet.create({
    global: {
        height: Dimensions.get("window").height ,
        backgroundColor: '#F8F8FA',
    },

    container: {
        flex: 1,
        width: '100%',
        marginTop: 0,
        borderBottomWidth: 1,
        borderColor: '#bfbfbf',
        minHeight: 50,
        flexDirection: "row",
        alignItems: 'center',
        paddingLeft: 10,
    }, 

    dataContainer: {
        marginTop: 15
    },

    profileInfo: {
        padding: 15,
        backgroundColor: '#FFF',
        borderBottomWidth: 1,
        borderColor: '#eee'
    },

    name: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#333',
        marginTop: 5
    },

    nameRead: {
        fontSize: 18,
        color: '#ccc',
        marginTop: 5,
    },

    data: {
        fontSize: 13,
        color: '#999',
        marginTop: 5
    },

    nameItem: {
        fontSize: 19,
        color: '#858585',
        fontWeight: 'bold',
    },

    iconArrow: {
        position: "absolute",
        top: 10,
        right: 6,
        color: '#858585',
    },  

    listNameText: {
        flex: 1,
        borderBottomWidth: 1,
        fontSize: 20,
        paddingLeft: 10
    },

    addContainer: {
        flexDirection: "row",
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },

    buttonAdd: {
        height: 40,
        width: 40,
        marginBottom: 8
    },  

    containerSpinner: {
        backgroundColor: '#000000',
        opacity: 0.2,
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
    },

    spinner: {
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
        justifyContent: 'center', 
        alignItems: 'center',
        opacity: 1 ,
        marginBottom: 170,
    },




    info: {
        padding: 15,
        justifyContent: "center",
        textAlign: "center",
        borderRadius: 30,
        fontSize: 18
    },    

    refreshContainer: {
        marginTop: 10,
        flexDirection: "row",
        justifyContent: "center",
    },

    errorContainer: {
        padding: 15,
        width: '100%',
        alignItems: "center",
        backgroundColor: '#e37f7f',
        justifyContent: "center",
        marginTop: 49
    },

    error: {
        padding: 15,
        justifyContent: "center",
        textAlign: "center",
        borderRadius: 30,
        fontSize: 18
    }, 
});