import React, { Component } from 'react';
import { View, StyleSheet, Text, FlatList, TextInput,
        ActivityIndicator, Dimensions, AsyncStorage, 
        TouchableOpacity } from 'react-native';   
import Button from '../../Button';        
import ListItem from '../../ListItem';
import Icon from 'react-native-vector-icons/FontAwesome';
import api from '../../../services/api';
import Moment from 'moment';
import { Feather } from '@expo/vector-icons';

export default class List extends Component {
    state = {
        listItens : [],
        errorMessage : null,
        infoMessage : null,
        loading: false,
        loadingInfinite: false,
        total: 1,
        dataInicial: '',
        dataFinal: '',
        marcacao: []
    };
    _isMounted = false;

    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

	_bootstrapAsync = async () => {
    }

    componentDidMount() {
        this._isMounted = true;

        this._unsubscribe = this.props.navigation.addListener('focus', () => {      
            if (this._isMounted) {
                this.loadItensList();
            }
        });          
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    
    loadItensList = async () => {
        this.setState({ errorMessage: null });
        if (this.state.marcacao.length == 0) {
            try {
                this.setState({ loading: true });
                this.setState({ marcacao: [] });
                    
                let url = 'https://qdqb0dcuqk.execute-api.us-east-1.amazonaws.com/default/CorpApp-ListClockEvent';
                let data = await api.get(url);
                console.log(data);

                this.setState({ marcacao: data.data.content });
                this.setState({ dataInicial: data.data.dataInicial });
                this.setState({ dataFinal: data.data.dataFinal });
                this.setState({ loading: false });
            } catch (error) {
                console.log('Error getting user:', error.originalError.message);
                this.setState({ errorMessage: error.originalError.message });
                this.setState({ loading: false });
            }
        }
    }              
    /*                    
    <ListItem 
        key={item.id} 
        item={item} 
        navigation={this.props.navigation} 
    />      */  

    render() {
        return (
            <View style={styles.global}>
                <View>
                    <View style={styles.itensListContainer}>
                        <FlatList                                                  
                            data={this.state.marcacao}  
                            keyExtractor={item => String(item.dataApuracao)}
                            numColumns={1}
                            bounces={false}
                            renderItem={({item: item}) => (       
                                <>                         
                                <View style={styles.container}>                                                             
                                    <Text style={styles.title}>{Moment(item.dataApuracao).format('DD/MM/YYYY')}</Text>

                                </View>

                         
                                <View style={styles.containerItem}>   
                                    <FlatList                                                  
                                    data={item.situacoesApuradas}  
                                    keyExtractor={item => String(item.id)}
                                    numColumns={1}
                                    bounces={false}
                                    renderItem={({item: item}) => (                                
                                                                                                
                                        <Text>{item.quantidadeHoras} - {item.situacao.descricao}</Text>
                                    
                                    )}
                                    />
                                </View>
                                </>
                            )}
                        />
                    </View>    

                    { this.state.marcacao.length == 0 && !this.state.loading && !this.state.errorMessage &&
                        <Text style={styles.info}>
                            Nenhuma marcação disponível{"\n"}
                            no perído{"\n"}
                            {Moment(this.state.dataInicial).format('DD/MM/YYYY')} -  {Moment(this.state.dataFinal).format('DD/MM/YYYY')}
                        </Text>
                    }

                    { this.state.errorMessage && 
                        <View style={styles.errorContainer}>
                            <Text style={styles.error}>{this.state.errorMessage}</Text>

                            <TouchableOpacity 
                                onPress={this.loadItensList}
                                style={styles.refreshContainer}>
                                <Feather name="refresh-cw" size={20}/>
                                <Text style={{ fontSize: 20, marginLeft: 6 }}>Tentar novamente</Text>
                            </TouchableOpacity>

                        </View>    
                    }
                    { this.state.infoMessage &&
                        <Text style={styles.info}>{this.state.infoMessage}</Text>
                    }
                </View>

                { this.state.loading &&
                    <View style={styles.containerSpinner}>     
                    </View>    
                }
                
                { this.state.loading && 
                    <ActivityIndicator 
                        style={styles.spinner}
                        size="large" 
                        color="#000000" 
                    />       
                }
            </View>
        )    
    }
}

const styles = StyleSheet.create({
    global: {
        height: Dimensions.get("window").height ,
        backgroundColor: '#F8F8FA',
    },
    container: {
        paddingTop: 10,
        backgroundColor: '#FFF',
        borderTopWidth: 1,
        borderColor: '#eee',
        flexDirection: "row",
        justifyContent: "center",
    },
    containerItem: {
        paddingBottom: 15,
        paddingTop: 10,
        backgroundColor: '#FFF',
        borderColor: '#eee',
        flexDirection: "row",
        justifyContent: "center",
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#333',
        marginTop: 5
    },
    itensListContainer: {
        marginTop: 15
    },
    containerSpinner: {
        backgroundColor: '#000000',
        opacity: 0.2,
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
    },
    errorContainer: {
        padding: 15,
        width: '100%',
        alignItems: "center",
        backgroundColor: '#e37f7f',
        justifyContent: "center"
    },
    refreshContainer: {
        marginTop: 10,
        flexDirection: "row",
        justifyContent: "center",
    },
    info: {
        padding: 15,
        justifyContent: "center",
        textAlign: "center",
        borderRadius: 30,
        fontSize: 18
    },    
    spinner: {
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
        justifyContent: 'center', 
        alignItems: 'center',
        opacity: 1 ,
        marginBottom: 170,
    },

    listNameText: {
        flex: 1,
        borderBottomWidth: 1,
        fontSize: 20,
        paddingLeft: 10
    },

    addContainer: {
        flexDirection: "row",
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },

    buttonAdd: {
        height: 40,
        width: 40,
        marginBottom: 8
    },
});