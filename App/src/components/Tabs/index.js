import React, { Component, useState } from 'react'
import { View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-native-safe-area-context';   

/*
*/

//<SafeAreaView style={{marginTop: -50}} forceInset={{'top': 'never'}}>
export default function Tabs ({ nav }) {
//const Tabs = ({nav}) => (
//function Tabs({navigationRef}) {
    const [selectedMenu, setSelectMenu] = useState('Aniversariantes');

    async function click(nav, menu) {
        await setSelectMenu(menu);
        nav.current.navigate(menu);
    }
    
    return (

        <SafeAreaView style={{marginTop: -50}} forceInset={{'top': 'never'}}>
        <View style={styles.container}>        
            <TouchableOpacity style={[ selectedMenu === 'Aniversariantes' ? styles.main : styles.iconView]} 
                onPress={ () => click(nav, 'Aniversariantes')}>
                <Icon name="birthday-cake" size={22} style={[ selectedMenu === 'Aniversariantes' ? styles.mainicon : styles.icon ]}/> 
            </TouchableOpacity>

            <TouchableOpacity style={[ selectedMenu === 'Marcações' ? styles.main : styles.iconView]} 
                onPress={ () => click(nav, 'Marcações')}>
                <Icon name="clock-o" size={22} style={[ selectedMenu === 'Marcações' ? styles.mainicon : styles.icon ]}/> 
            </TouchableOpacity>
            
            <TouchableOpacity style={[ selectedMenu === 'Notificações' ? styles.main : styles.iconView]} 
                onPress={ () => click(nav, 'Notificações')}
            >
                <Icon name="bell" size={22} style={[ selectedMenu === 'Notificações' ? styles.mainicon : styles.icon]}/>
            </TouchableOpacity>            

            <TouchableOpacity style={[ selectedMenu === 'Usuário' ? styles.main : styles.iconView]}
                onPress={() => click(nav, 'Usuário')}
            >
                <Icon name="user-o" size={22} style={[ selectedMenu === 'Usuário' ? styles.mainicon : styles.icon]}/>
            </TouchableOpacity>    
        </View>
        </SafeAreaView>
    )
    
}

/*

        <TouchableOpacity style={styles.main}>
            <Icon name="plus" size={20} style={styles.mainicon}/>
        </TouchableOpacity> 

        <TouchableOpacity style={styles.iconView}>
            <Icon name="bell-o" size={20} style={styles.icon}/>
        </TouchableOpacity>
*/

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        height: 50,
        borderTopWidth: 1,
        borderColor: '#eee',
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        
    },

    icon: {
      color: '#C0C0C0'
    },

    iconView: {
        flexDirection: "row",
        justifyContent: "space-around",
        width: '20%',
    },
  
    active: {
      color: '#03c297'
    },

    main: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: '#03c297',
        justifyContent: "center",
        alignItems: "center"
    },

    mainicon: {
        color: '#FFF'
    }
});

//export default Tabs
